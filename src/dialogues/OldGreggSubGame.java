package dialogues;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Code for the sub game.
 */
public class OldGreggSubGame {
	/** 2D array used to represent the game. */
	private int[][] subBoard;
	/** The current row the player occupies. */
	private int currentRow;
	/** The current column the player occupies. */
	private int currentCol;
	/** The current status of the game. */
	private Status gameStatus;
	/** The total number of rows in the board. */
	private static final int BOARD_ROWS = 5;
	/** The total number of columns in the board. */
	private static final int BOARD_COLS = 128;
	/** The column to end the game on. */
	private static final int FINISH_LINE = 120;
	/** The value for the end. */
	private static final int END = 3;

	/**
	 * Creates the starting conditions.
	 */
	public OldGreggSubGame() {
		gameStatus = Status.PLAYING;
		currentRow = 2;
		currentCol = 0;
		subBoard = createSubBoard();
	}

	/**
	 * Builds the game board.
	 *
	 * @return newBoard The board for the game.
	 */
	public final int[][] createSubBoard() {
		int[][] newBoard = new int[BOARD_ROWS][BOARD_COLS];
		readData("SubGrid.txt", newBoard);
		for (int i = FINISH_LINE; i < BOARD_COLS; i++) {
			for (int j = 0; j < BOARD_ROWS; j++) {
				newBoard[j][i] = END;
			}
		}
		return newBoard;
	}

	/**
	 * Reads in a file of the borders.
	 * 
	 * @param filename The file containing the border info.
	 * @param board The array to store the data.
	 */
	public final void readData(final String filename, final int[][] board) {
		try {
            // open the data file
			Scanner fileReader = new Scanner(new File(filename));

            int count = 0;
            while (fileReader.hasNext()) {
                int col = count / BOARD_ROWS;
                int row = count % BOARD_ROWS;
            	board[row][col] = fileReader.nextInt();
                count++;
            }
            fileReader.close();
        } catch (FileNotFoundException error) {
            System.out.println("File not found ");
        }
	}
	
	/**
	 * Moves the player through the board.
	 */
	public final void move() {
		if (subBoard[currentRow][currentCol] == 0
				&& currentCol <= FINISH_LINE) {
			subBoard[currentRow][currentCol] = 1;
		}
	}

	/**
	 * Checks if the player collides with an obstacle.
	 */
	public final void collisionCheck() {
		if (subBoard[currentRow][currentCol] != 0) {
			gameStatus = Status.LOSER;
		}
	}

	/**
	 * Checks if the player reaches the finish line.
	 */
	public final void gameFinish() {
		if (currentCol == FINISH_LINE) {
			gameStatus = Status.WINNER;
		}
	}

	/**
	 * Sets the game back to the initial conditions.
	 */
	public final void reset() {
		gameStatus = Status.PLAYING;
		currentRow = 2;
		currentCol = 0;
		subBoard = createSubBoard();
	}

	/**
	 * Returns the value of the board at a location.
	 *
	 * @param row The row to check.
	 * @param col The column to check.
	 *
	 * @return subBoard[row][col] The value at that location.
	 */
	public final int getBoardData(final int row, final int col) {
		return subBoard[row][col];
	}

	/**
	 * Returns the current row the player occupies.
	 *
	 * @return currentRow The player's row.
	 */
	public final int getCurrentRow() {
		return currentRow;
	}

	/**
	 * Sets the player's row.
	 *
	 * @param row The row to import.
	 */
	public final void setCurrentRow(final int row) {
		this.currentRow = row;
	}

	/**
	 * Returns the current column the player occupies.
	 *
	 * @return currentCol The player's column.
	 */
	public final int getCurrentCol() {
		return currentCol;
	}

	/**
	 * Sets the player's column.
	 *
	 * @param col The column to import.
	 */
	public final void setCurrentCol(final int col) {
		this.currentCol = col;
	}

	/**
	 * Returns the current game status.
	 *
	 * @return gameStatus The game's status.
	 */
	public final Status getGameStatus() {
		return gameStatus;
	}

	/**
	 * Sets the player's status.
	 *
	 * @param stat The status to import.
	 */
	public final void setGameStatus(final Status stat) {
		this.gameStatus = stat;
	}
}