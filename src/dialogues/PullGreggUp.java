package dialogues;


import java.awt.FlowLayout;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.WindowConstants;



/*********************************************************************
 * This has the user play a game that makes forces the user to try and
 * pull up our friend old gregg.
 * @author Brentan Chesla
 * @version 10/18/16
 */
@SuppressWarnings("serial")
public class PullGreggUp extends JFrame implements ActionListener {

  /** text field to show user how many clicks they did. */
  private TextField text;

  /** Holds the time for the timer. **/
  private int old;

  /** button to iterate the clicks. */
  private JButton pressme;

  /** number of clicks done by user. */
  private int numClicks;

  /** Timer to keep time. */
  private Timekeeper timer;

  /** Size of text box. */
  private static final int TEXT_FIELD = 20;

  /** Clicks to win. */
  private static final int CLICKS_TO_WIN = 50;

  /** Deduction from play. **/
  private static final int TOTAL_DEDUCTION = 10;

  /** For the timer to decide if it exists. **/
  private boolean firstorno;

  /** Seconds to play. */
  private static final int SECONDS_TILL_DEDUCTION = 2;

  /** Size of next play. */
  private static final int SIZEX = 350;

  /** Size of next play. */
  private static final int SIZEY = 100;

  /*********************************************************************
   * This is the constructor method that accepts a users param
   * request and builds the game.
   * @param title this is the title of the frames
   */
  public PullGreggUp(final String title) {
    super(title);
    //
    //    setSize(SIZEX, SIZEY);
    //    setVisible(true);
    System.out.println("Upon fishing....Howard Feels a Snag");
    System.out.println("He is unable to pull his catch up right away however.");

    old = 0;
    firstorno = false;
    timer = new Timekeeper();
    text = new TextField(TEXT_FIELD);
    numClicks = 0;
    pressme = new JButton("Pull him up with your big strong arms!");
    setLayout(new FlowLayout());
    setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    add(text);
    add(pressme);
    pressme.addActionListener(this);



  }
  /**
   * Returns number of clicks.
   * @return numClicks returns total clicks
   */
  public final int getnumclick() {
    return numClicks;
  }

  /****
   * click the button for testing.
   */
  public final void clickit() {
    pressme.doClick();
  }

  /***
   * For testing purposes to end the game.
   */
  public final void setendit() {
    numClicks = CLICKS_TO_WIN;
    pressme.doClick();
  }

  /*********************************************************************
   * This is our action listener that counts how many clicks a user has.
   * However, if the user cannot play well enough, he/she will lose.
   * @param playingfortheLadies the action of clicking the button
   */
  public final void actionPerformed(final ActionEvent playingfortheLadies) {
    //holder to deduct points
    numClicks++;
    if (!firstorno) {
      firstorno = true;
      timer.start();
    }

    if (timer.getSeconds() > old + SECONDS_TILL_DEDUCTION 
        && !(numClicks > CLICKS_TO_WIN)) {
      numClicks = numClicks - TOTAL_DEDUCTION;
      old = timer.getSeconds() + SECONDS_TILL_DEDUCTION;
      System.out.println(old);
    }
    if (numClicks > CLICKS_TO_WIN) {
      setVisible(false); //you can't see me!
      dispose(); //Destroy the JFrame object
      System.out.println("It is time....to meet....old gregg!");
    new EnterOlGregg();

    }
    text.setText("" + numClicks);
  }
  /********
   * 
   * Main method to run game.
   * @param args args to run game.
   */
  public static void main(final String[] args) {
    PullGreggUp play = new PullGreggUp("Neat");
    play.setSize(SIZEX, SIZEY);
    play.setVisible(true);
  }
}