package dialogues;

import java.util.Scanner;

/*********************************************************************
 * This Program is a MiniGame that you are tasked with finding skill that will,
 * help Howard hopefully become famous. The task is long, can you find the 
 * skill Howard needs?
 * @author Brentan Chesla
 * @version 10/18/16
 */
public class FindSkillItem {
  
  /** Out of bounds. */
  private static final int OUT_OF_BOUNDS = -1;
  
  /** Arraysize. */
  private static final int ARRAY_SIZE = 10;
  
  /** Instantiates the maze. */
  private Spotholder[][] theLostMaze;
  
  /** Instantiates the wall. */
  private Spotholder wall;
  
  /** Instantiates an empty spot. */
  private Spotholder nothing;
  
  /** Instiates the item to be found. */
  private Spotholder thehiddenItem;
  
  /** current location on x plane. */
  private int currentx;
  
  /** current location on y plane. */
  private int currenty;

  /** numbering system for array. */
  private static final int ZERO = 0;
  
  /** numbering system for array. */
  private static final int ONE = 1;
  
  /** numbering system for array. */
  private static final int TWO = 2;
  
  /** numbering system for array. */
  private static final int THREE = 3;
  
  /** numbering system for array. */
  private static final int FOUR = 4;
  
  /** numbering system for array. */
  private static final int FIVE = 5;
  
  /** numbering system for array. */
  private static final int SIX = 6;
  
  /** numbering system for array. */
  private static final int SEVEN = 7;
  
  /** numbering system for array. */
  private static final int EIGHT = 8;
  
  /** numbering system for array. */
  private static final int NINE = 9;
  
  /** Size of next play. */
  private static final int SIZEX = 350;
  
  /** Size of next play. */
  private static final int SIZEY = 100;

  /*********************************************************************
   * This is the Constructor method that helps build the game.
   ********************************************************************/
  public FindSkillItem() {
    currentx = 0;
    currenty = 0;
    theLostMaze = new Spotholder[ARRAY_SIZE][ARRAY_SIZE];
    wall = new Spotholder();
    nothing = new Spotholder();
    thehiddenItem = new Spotholder();

    thehiddenItem.setItem(true);
    thehiddenItem.setWall(false);
    wall.setWall(true);
    createMap();



    run();

  }
  
  /***
   * Getter Method the helps retreive current locations.
   * Used Primarily for visuals.
   * @return currentx Current x Location
   */
  public final int getx() {
    return currentx;
  }
  
  /***
   * Getter Method the helps retreive current locations.
   * Used Primarily for visuals.
   * @return currenty Current y Location
   */
  public final int gety() {
    return currenty;
  }
  
  /*******************************************************************
   * This Method takes the program and sends it into a loop, asking the user
   * if he/she wants to go Left, Right, Up, or Down. It will check for
   * Walls as well as check for the skill Item.
   *******************************************************************/
  public final void run() {
    Boolean keeprunning = true;
    Scanner sc = new Scanner(System.in);
    String user = "";
    String valid;
    Boolean validrequest;

    while (keeprunning) {
      validrequest = false;
      valid = "";
      System.out.println("What do you want to go?");
      System.out.println("Options: Left, Right, Up, Down");
      user = sc.nextLine();
      //testing only!
      if (user.equals("Left test")) {
        keeprunning = false;
        user = "Left";
      }
      
    //testing only!
      if (user.equals("Right test")) {
        keeprunning = false;
        user = "Right";
      }
      
      if (user.equals("done")) {
        keeprunning = false;
        validrequest = true;
        valid = "done";
        
      }
      
    //testing only!
      if (user.equals("Up test")) {
        keeprunning = false;
        user = "Up";
      }
      
    //testing only!
      if (user.equals("Down test")) {
        keeprunning = false;
        user = "Down";
      }
      if (user.equals("Left")) {
        valid = checkSpot(currentx - 1, currenty);
        validrequest = true;
      } else {
        if (user.equals("Right")) {
          valid = checkSpot(currentx + 1, currenty);
          validrequest = true;
        } else {
          if (user.equals("Up")) {
            valid = checkSpot(currentx, currenty - 1);
            validrequest = true;
          } else {
            if (user.equals("Down")) {
              valid = checkSpot(currentx, currenty + 1);
              validrequest = true;
            }
          }
        }
      }

      if (validrequest) {
        if (valid == "wall") {
          System.out.println("Howard goes to his " + user  
              + " and finds a wall. He cannot go this way.");
        } else {
          if (valid == "nothing") {
            System.out.println("Howard continues his journey.... ");
          } else {
            if (valid == "done") {
              System.out.println("Howard finds the skill to play!!! "
                  + "Hopefully it is good enough.");

              System.out.println("We will play at a live music"
                  + " festival to see how well you play");
              System.out.println();
              sc.close();
              PressButton play = new PressButton("Click it");
              play.setSize(SIZEX, SIZEY);
              play.setVisible(true);
              keeprunning = false;
              break;
            }
          }
        }
      } else {
        System.out.println("Howard could not understand your directions! :(");
      }
      System.out.println("---------------------------"
          + "---------------------------------");
      System.out.println();

      System.out.println(currentx + ", " + currenty);
    }
  }


  /********************************************************************
   * This Method takes the users desired path and checks to see if it
   * is valid.
   * @param xlocFind This is the desired x direction.
   * @param ylocFind This is the desired y direction.
   * @return "done" if the user found the Skill. Else "nothing" or "wall"
   */
  public final String checkSpot(final int xlocFind, final int ylocFind) {
    Spotholder temp;

    if (xlocFind < ARRAY_SIZE && xlocFind > OUT_OF_BOUNDS  
        && ylocFind < ARRAY_SIZE && ylocFind > OUT_OF_BOUNDS) {
      temp = theLostMaze[xlocFind][ylocFind];

      if (!temp.getWall()) { 
        currentx = xlocFind;
        currenty = ylocFind;

        //found item
        if (temp.getItem()) {
          return "done";
        } else {

          //didn't find item
          return "nothing";
        }
      } else {
        return "wall";
      }
    } else {
      return "wall";
    }
  }

  
  
  /********************************************************************
   * This Method creates our map and hidden location of Ramsey.
   */
  public final void createMap() {

    
    
    //Column 0
    for (int y = 0; y < ARRAY_SIZE; y++) {
      theLostMaze[0][y] = nothing;
    }

    //Column 1
    for (int y = 0; y < ARRAY_SIZE; y++) {

      if (y == TWO || y == EIGHT) {
        theLostMaze[1][y] = nothing;
      } else {
        theLostMaze[1][y] = wall;
      }
    }

    //Column 2
    for (int y = 0; y < ARRAY_SIZE; y++) {
      if (y == SIX) {
        theLostMaze[TWO][SIX] = wall;
      } else {
        theLostMaze[TWO][y] = nothing;
      }
    }

    //Column 3
    for (int y = 0; y < ARRAY_SIZE; y++) {
      if (y == SEVEN || y == NINE || y == ZERO || y == TWO) {
        theLostMaze[THREE][y] = nothing;
      } else {

        theLostMaze[THREE][y] = wall;
      }
    }

    //Column 4
    for (int y = 0; y < ARRAY_SIZE; y++) {
      if (y == ONE || y == SIX || y == SEVEN || y == EIGHT) {
        theLostMaze[FOUR][y] = wall;
      } else {
        theLostMaze[FOUR][y] = nothing;
      }
    }

    // Column 5 
    for (int y = 0; y < ARRAY_SIZE; y++) {
      if (y == ZERO || y == ONE || y == THREE || y == FOUR) {
        theLostMaze[FIVE][y] = wall;
      } else {
        theLostMaze[FIVE][y] = nothing;
      }
    }

    //Column 6

    for (int y = 0; y < ARRAY_SIZE; y++) {
      if (y == ONE || y == TWO || y == SIX || y == EIGHT) {
        theLostMaze[SIX][y] = wall;
      } else {
        theLostMaze[SIX][y] = nothing;
      }
    }

    //Column 7
    for (int y = 0; y < ARRAY_SIZE; y++) {
      if (y == TWO || y == FOUR || y == FIVE || y == SIX || y == EIGHT) {
        theLostMaze[SEVEN][y] = wall;
      } else {
        theLostMaze[SEVEN][y] = nothing;
      }
    }

    //Column 8
    for (int y = 0; y < ARRAY_SIZE; y++) {
      if (y == ZERO || y == FOUR || y == FIVE || y == SIX) {
        theLostMaze[EIGHT][y] = wall;
      } else {
        theLostMaze[EIGHT][y] = nothing;

      }
    }

    //Column 9
    for (int y = 0; y < ARRAY_SIZE; y++) {
      if (y == TWO || y == SIX || y == SEVEN || y == EIGHT || y == NINE) {
        theLostMaze[NINE][y] = wall;
      } else {
        if (y == FIVE) {
          theLostMaze[NINE][y] = thehiddenItem;
        } else {
          theLostMaze[NINE][y] = nothing;
        }
      }
    }
  }
  /********
   * 
   * Main method to run game.
   * @param args args to run game.
   */
  public static void main(final String[] args) {
    
    new FindSkillItem();
  }
}
