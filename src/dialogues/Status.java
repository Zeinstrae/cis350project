package dialogues;
/**
 * Status values for the game.
 */
public enum Status {
	/** Possible statuses of a game. */
	WINNER, LOSER, PLAYING
}
