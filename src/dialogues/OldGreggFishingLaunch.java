package dialogues;

import java.awt.Dimension;
import javax.swing.JFrame;

/**
 * Main method.
 */
public final class OldGreggFishingLaunch {
	
	/**
	 * Throws error if someone tries to instantiate Main.
	 */
	public OldGreggFishingLaunch() {
		final int width = 1450;
		final int height = 875;
		//final Status status = Status.PLAYING;
		JFrame frame = new JFrame("The adventures of Old Gregg");
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
			frame.getContentPane().add(new OldGreggFishingView());
			
			frame.setPreferredSize(new Dimension(width, height));
			frame.setSize(frame.getPreferredSize());
			frame.setVisible(true);
	}
}
