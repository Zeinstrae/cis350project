package dialogues;

import java.util.Timer;
import java.util.TimerTask;

/*********************************************************************
 * This Class helps PressButton keep time for the button presses.
 * @author Brentan Chesla
 * @version 10/18/16
 */
public class Timekeeper {

  /** number of milliseconds in second. */
  private static final int MILLI = 1000;
  
  /** Total time past in seconds. */
  private int secondsPassed;
  
  /** The Timer. */
  private Timer timer;
  
  /** Specifices what seconds mean. */
  private TimerTask task;

  /*********************************************************************
   * Constructor to make the clock and tasks in which to count.
   */
  public Timekeeper() {
    secondsPassed = 0;
    timer = new Timer();
    task = new TimerTask() {
      public void run() {
        secondsPassed++;
      }
    };
  }



  /********************************
   * This Starts the clock.
   * 
   */
  public final void start() {
    timer.scheduleAtFixedRate(task, MILLI, MILLI);

  }

  /*******************************
   * This method returns the seconds passed.
   * @return secondsPassed total number of seconds
   */
  public final int getSeconds() {
    return secondsPassed;
  }
}
