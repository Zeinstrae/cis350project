package dialogues;

import org.junit.Test;
import org.junit.Ignore;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Scanner;

public class OldGreggTest {
	
	@Test
	public void testFishingConstructor() {
		OldGreggFishingGame game = new OldGreggFishingGame();
		assertEquals(game.getNextRow(), 5);
	}
	
	@Test
	public void testSubConstructor() {
		OldGreggSubGame game = new OldGreggSubGame();
		assertEquals(game.getCurrentRow(), 2);
	}
	
	@Test
	public void testPacConstructor() {
		OldGreggPacGame game = new OldGreggPacGame();
		assertEquals(game.getNextRow(), 5);
	}
	
	@Test
	public void testCreateBoard() {
		OldGreggFishingGame game = new OldGreggFishingGame();
		assertEquals(game.getBoardData(5, 5), 1);
	}
	
	@Test
	public void testPlaceFish() {
		OldGreggFishingGame game = new OldGreggFishingGame();
		game.setCurrentFish(4);
		game.placeFish();
		assertEquals(game.getBoardData(5, 5), 1); //What is a better test condition?
	}
	
	@Test
	public void testPlaceFishOverlap() {
		OldGreggFishingGame game = new OldGreggFishingGame();
		game.setCurrentFish(2);
		for(int i = 0; i < 10; i++){
			for (int j = 0; j < 10; j++){
				if (game.getBoardData(i, j) == 0){
					game.setBoardData(i, j, 2);
				}
			}
		}
		game.setBoardData(9, 9, 0);
		game.placeFish();
		assertEquals(game.getBoardData(5, 5), 1); //What is a better test condition?
	}
	
	@Test
	public void testWinningFinish() {
		OldGreggFishingGame game = new OldGreggFishingGame();
		game.setFish(0);
		game.setAIFish(120);
		game.gameFinish();
		assertEquals(game.getGameStatus(), Status.WINNER);
	}
	
	@Test
	public void testLosingFinish() {
		OldGreggFishingGame game = new OldGreggFishingGame();
		game.setFish(300);
		game.setAIFish(120);
		game.gameFinish();
		assertEquals(game.getGameStatus(), Status.LOSER);
	}
	
	@Test
	public void testSubFinish() {
		OldGreggSubGame game = new OldGreggSubGame();
		game.setCurrentCol(120);
		game.gameFinish();
		assertEquals(game.getGameStatus(), Status.WINNER);
	}
	
	@Test
	public void testFishingMoveFishable() {
		OldGreggFishingGame game = new OldGreggFishingGame();
		game.setBoardData(5, 5, 3);
		game.setNextCol(4);
		game.move();
		assertEquals(game.getBoardData(5, 5), 2);
	}
	
	@Test
	public void testFishingMoveFish() {
		OldGreggFishingGame game = new OldGreggFishingGame();
		game.setBoardData(5, 4, 2);
		game.setNextCol(4);
		game.move();
		assertEquals(game.getBoardData(5, 4), 3);
	}
	
	@Test
	public void testFishingMoveEmpty() {
		OldGreggFishingGame game = new OldGreggFishingGame();
		game.setBoardData(5, 4, 0);
		game.setNextCol(4);
		game.move();
		assertEquals(game.getBoardData(5, 4), 1);
	}
	
	@Test
	public void testSubMoveValid() {
		OldGreggSubGame game = new OldGreggSubGame();
		game.setCurrentCol(3);
		game.setCurrentRow(0);
		game.move();
		assertEquals(game.getBoardData(0, 3), 1);
	}
	
	@Test
	public void testFishingReset() {
		OldGreggFishingGame game = new OldGreggFishingGame();
		game.setNextRow(4);
		game.move();
		game.reset();
		assertEquals(game.getBoardData(5, 5), 1);
	}
	
	@Test
	public void testSubReset() {
		OldGreggSubGame game = new OldGreggSubGame();
		game.setCurrentCol(3);
		game.setCurrentRow(0);
		game.reset();
		assertEquals(game.getBoardData(2, 0), 1);
	}
	
	@Test
	public void testCollisionCheck() {
		OldGreggSubGame game = new OldGreggSubGame();
		game.setCurrentCol(6);
		game.setCurrentRow(2);
		game.collisionCheck();
		assertEquals(game.getGameStatus(), Status.LOSER);
	}
	
	@Test
	public void testGetFish() {
		OldGreggFishingGame game = new OldGreggFishingGame();
		game.setFish(20);
		int fish = game.getFish();
		assertEquals(fish, 20);
	}
	
	@Test
	public void testGetAIFish() {
		OldGreggFishingGame game = new OldGreggFishingGame();
		game.setAIFish(200);
		int fish = game.getAIFish();
		assertEquals(fish, 200);
	}
	
	@Test
	public void testGetCurrentFish() {
		OldGreggFishingGame game = new OldGreggFishingGame();
		game.setCurrentFish(6);
		int fish = game.getCurrentFish();
		assertEquals(fish, 6);
	}
	
	@Test
	public void testFishingSetGameStatus() {
		OldGreggFishingGame game = new OldGreggFishingGame();
		game.setGameStatus(Status.LOSER);
		assertEquals(game.getGameStatus(), Status.LOSER);
	}
	
	@Test
	public void testSubSetGameStatus() {
		OldGreggSubGame game = new OldGreggSubGame();
		game.setGameStatus(Status.WINNER);
		assertEquals(game.getGameStatus(), Status.WINNER);
	}
	
	@Test
	public void testGetNextCol() {
		OldGreggFishingGame game = new OldGreggFishingGame();
		game.setNextCol(3);
		assertEquals(game.getNextCol(), 3);
	}
	
	@Test
	public void testSubGetCurrentCol() {
		OldGreggSubGame game = new OldGreggSubGame();
		game.setCurrentCol(3);
		assertEquals(game.getCurrentCol(), 3);
	}
	
	@Test
	public void tesPacMoveLeftValid() {
		OldGreggPacGame game = new OldGreggPacGame();
		game.setBoardData(5, 5, 1);
		game.setNextCol(4);
		game.move();
		assertEquals(game.getBoardData(5, 5), 0);
	}
	
	@Test
	public void tesPacMoveLeftInvalid() {
		OldGreggPacGame game = new OldGreggPacGame();
		game.setBoardData(3, 3, 1);
		game.setNextCol(2);
		game.move();
		assertEquals(game.getBoardData(3, 3), 1);
	}
	
	@Test
	public void tesPacMoveRightValid() {
		OldGreggPacGame game = new OldGreggPacGame();
		game.setBoardData(5, 5, 1);
		game.setNextCol(6);
		game.move();
		assertEquals(game.getBoardData(5, 5), 0);
	}
	
	@Test
	public void tesPacMoveRightInvalid() {
		OldGreggPacGame game = new OldGreggPacGame();
		game.setBoardData(6, 6, 1);
		game.setNextCol(7);
		game.move();
		assertEquals(game.getBoardData(6, 6), 1);
	}
	
	@Test
	public void tesPacMoveUpValid() {
		OldGreggPacGame game = new OldGreggPacGame();
		game.setBoardData(5, 5, 1);
		game.setNextRow(4);
		game.move();
		assertEquals(game.getBoardData(5, 5), 0);
	}
	
	@Test
	public void tesPacMoveUpInvalid() {
		OldGreggPacGame game = new OldGreggPacGame();
		game.setBoardData(3, 3, 1);
		game.setNextRow(2);
		game.move();
		assertEquals(game.getBoardData(3, 3), 1);
	}
	
	@Test
	public void tesPacMoveDownValid() {
		OldGreggPacGame game = new OldGreggPacGame();
		game.setBoardData(5, 5, 1);
		game.setNextRow(6);
		game.move();
		assertEquals(game.getBoardData(5, 5), 0);
	}
	
	@Test
	public void tesPacMoveDownInvalid() {
		OldGreggPacGame game = new OldGreggPacGame();
		game.setBoardData(3, 3, 1);
		game.setNextRow(4);
		game.move();
		assertEquals(game.getBoardData(3, 3), 1);
	}
	

	  @Test
	  public void testLeftRamsey() {
	    String input = "Left test";
	    InputStream in = new ByteArrayInputStream(input.getBytes());
	    System.setIn(in);
	    FindRamsey random = new FindRamsey();
	    
	    
	    if (random.getx() == 0 && random.gety() == 0) {
	      assertTrue(true);
	    } else {
	      assertTrue(false);
	    }
	    random.setrunfalse();
	  }
	  
	  @Test
	  public void testRightRamsey() {
	    String input = "Right test";
	    InputStream in = new ByteArrayInputStream(input.getBytes());
	    System.setIn(in);
	    FindRamsey random = new FindRamsey();
	    
	    
	    if (random.getx() == 1 && random.gety() == 0) {
	      assertTrue(true);
	    } else {
	      assertTrue(false);
	    }
	    random.setrunfalse();
	  }
	  
	  @Test
	  public void testDownRamsey() {
	    String input = "Down test";
	    InputStream in = new ByteArrayInputStream(input.getBytes());
	    System.setIn(in);
	    FindRamsey random = new FindRamsey();
	    
	    
	    if (random.getx() == 0 && random.gety() == 0) {
	      assertTrue(true);
	    } else {
	      assertTrue(false);
	    }
	    random.setrunfalse();
	  }
	  
	  @Test
	  public void testUpRamsey() {
	    String input = "Up test";
	    InputStream in = new ByteArrayInputStream(input.getBytes());
	    System.setIn(in);
	    FindRamsey random = new FindRamsey();
	    
	    
	    if (random.getx() == 0 && random.gety() == 0) {
	      assertTrue(true);
	    } else {
	      assertTrue(false);
	    }
	    random.setrunfalse();
	  }
	  
	  @Test
	  public void testStepZeroRamsey() {
	    String input = "Right test step";
	    InputStream in = new ByteArrayInputStream(input.getBytes());
	    System.setIn(in);
	    FindRamsey random = new FindRamsey();
	    
	    
	    if (random.getx() == 0 && random.gety() == 0) {
	      assertTrue(true);
	    } else {
	      assertTrue(false);
	    }
	    random.setrunfalse();
	  }
	  
	  @Test
	  public void testRamseyend() {
	    String input = "done";
	    InputStream in = new ByteArrayInputStream(input.getBytes());
	    System.setIn(in);
	    FindRamsey random = new FindRamsey();
	      assertTrue(true);
	  }
	  
	  
	  

	  @Test
	  public void testLeftSkill() {
	    String input = "Left test";
	    InputStream in = new ByteArrayInputStream(input.getBytes());
	    System.setIn(in);
	    FindSkillItem random = new FindSkillItem();
	    
	    
	    if (random.getx() == 0 && random.gety() == 0) {
	      assertTrue(true);
	    } else {
	      assertTrue(false);
	    }
	  }
	  
	  @Test
	  public void testRightSkill() {
	    String input = "Right test";
	    InputStream in = new ByteArrayInputStream(input.getBytes());
	    System.setIn(in);
	    FindSkillItem random = new FindSkillItem();
	    
	    
	    if (random.getx() == 0 && random.gety() == 0) {
	      assertTrue(true);
	    } else {
	      assertTrue(false);
	    }
	  }
	  
	  @Test
	  public void testDownSkill() {
	    String input = "Down test";
	    InputStream in = new ByteArrayInputStream(input.getBytes());
	    System.setIn(in);
	    FindSkillItem random = new FindSkillItem();
	    if (random.getx() == 0 && random.gety() == 1) {
	      assertTrue(true);
	    } else {
	      assertTrue(false);
	    }
	  }
	  
	  @Test
	  public void testUpSkill() {
	    String input = "Up test";
	    InputStream in = new ByteArrayInputStream(input.getBytes());
	    System.setIn(in);
	    FindSkillItem random = new FindSkillItem();
	    if (random.getx() == 0 && random.gety() == 0) {
	      assertTrue(true);
	    } else {
	      assertTrue(false);
	    }
	  }
	  
	  @Test
	  public void testskillend() {
	    String input = "done";
	    InputStream in = new ByteArrayInputStream(input.getBytes());
	    System.setIn(in);
	    FindSkillItem random = new FindSkillItem();
	      assertTrue(true);
	  }
	  
	  @Test
	  public void testbuttonwin() {
	    PressButton play = new PressButton("Neat");
	    play.setSize(350, 100);
	    play.setVisible(true);
	    play.setwinclick();
	    play.clickit();
	    assertTrue(true);
	  }
	  
	  @Test
	  public void testbuttonfail() {
	    PressButton play = new PressButton("Neat");
	    play.setSize(350, 100);
	    play.setVisible(true);
	    play.setendit();
	    play.clickit();
	    assertTrue(true);
	  }
	  
	  @Test
	  public void testbuttonclick() {
	    PressButton play = new PressButton("Neat");
	    play.setSize(350, 100);
	    play.setVisible(true);
	    play.clickit();
	    if (play.getnumclick() == 1) {
	    assertTrue(true);
	    } else {
	      assertTrue(false);
	    }
	  }
	  
	  @Test
	  public void testpullupclick() {
	    PullGreggUp play = new PullGreggUp("Neat");
	    play.setSize(350, 100);
	    play.setVisible(true);
	    play.clickit();
	    if (play.getnumclick() == 1) {
	    assertTrue(true);
	    } else {
	      assertTrue(false);
	    }
	  }
	  
	  @Test
	  public void testpullupwin() {
	    PullGreggUp play = new PullGreggUp("Neat");
	    play.setSize(350, 100);
	    play.setVisible(true);
	    play.setendit();
	    play.clickit();
	    assertTrue(true);
	  }
	 
}
