package dialogues;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Code for the sub searching game.
 */
public class OldGreggPacGame {
	/** 2D array used to represent the game. */
	private int[][] board;
	/** array to store top borders. */
	private int[] upBorder;
	/** array to store left borders. */
	private int[] leftBorder;
	/** array to store bottom borders. */
	private int[] downBorder;
	/** array to store right borders. */
	private int[] rightBorder;
	/** The current row the player occupies. */
	private int currentRow;
	/** The current column the player occupies. */
	private int currentCol;
	/** The row to move the player to. */
	private int nextRow;
	/** The column to move the player to. */
	private int nextCol;
	/** The current status of the game. */
	private Status gameStatus;
	/** The total number of rows in the board. */
	private static final int BOARD_ROWS = 10;
	/** The total number of columns in the board. */
	private static final int BOARD_COLS = 10;
	/** The starting location for the player. */
	private static final int PLAYER_START = 5;
	/** The value for the player. */
	private static final int SUB = 1;
	/** The value for an unexplored. */
	private static final int UNSCANNED = 2;
	/** Four. */
	private static final int FOUR = 4;
	
	/**
	 * Creates the starting conditions.
	 */
	public OldGreggPacGame() {
		gameStatus = Status.PLAYING;
		currentRow = PLAYER_START;
		nextRow = currentRow;
		currentCol = PLAYER_START;
		nextCol = currentCol;
		upBorder = new int[BOARD_ROWS * BOARD_COLS];
		leftBorder = new int[BOARD_ROWS * BOARD_COLS];
		downBorder = new int[BOARD_ROWS * BOARD_COLS];
		rightBorder = new int[BOARD_ROWS * BOARD_COLS];
		readData("PacGrid.txt");
		board = createBoard();
	}
	
	/**
	 * Reads in a file of the borders.
	 * 
	 * @param filename THe file containing the border info.
	 */
	public final void readData(final String filename) {
		try {
            // open the data file
			Scanner fileReader = new Scanner(new File(filename));

            int count = 0;
            while (fileReader.hasNext()) {
            	if (count % FOUR == 0) {
            		upBorder[count / FOUR] = fileReader.nextInt();
            	} else if (count % FOUR == 1) {
            		leftBorder[count / FOUR] = fileReader.nextInt();
            	} else if (count % FOUR == 2) {
            		downBorder[count / FOUR] = fileReader.nextInt();
            	} else {
            		rightBorder[count / FOUR] = fileReader.nextInt();
            	}
                count++;
            }
            fileReader.close();
        } catch (FileNotFoundException error) {
            System.out.println("File not found ");
        }
	}
	
	/**
	 * Builds the game board.
	 * 
	 * @return newBoard The board for the game.
	 */
	public final int[][] createBoard() {
		int[][] newBoard = new int[BOARD_ROWS][BOARD_COLS];
		for (int row = 0; row < BOARD_ROWS; row++) {
			for (int col = 0; col < BOARD_COLS; col++) {
				newBoard[row][col] = 2;		
			}
		}
		newBoard[currentRow][currentCol] = SUB;
		return newBoard;
	}
	
	/**
	 * Moves the player through the board.
	 */
	public final void move() {
		int value = (currentRow * BOARD_ROWS) + currentCol;
		if (nextRow > currentRow) {
			if (downBorder[value] == 0) {
				board[nextRow][nextCol] = SUB;
				board[currentRow][currentCol] = 0;
				currentRow = nextRow;
				currentCol = nextCol;
			}
		} else if (nextRow < currentRow) {
			if (upBorder[value] == 0) {
				board[nextRow][nextCol] = SUB;
				board[currentRow][currentCol] = 0;
				currentRow = nextRow;
				currentCol = nextCol;
			}
		} else if (nextCol > currentCol) {
			if (rightBorder[value] == 0) {
				board[nextRow][nextCol] = SUB;
				board[currentRow][currentCol] = 0;
				currentRow = nextRow;
				currentCol = nextCol;
			}
		} else if (nextCol < currentCol) {
			if (leftBorder[value] == 0) {
				board[nextRow][nextCol] = SUB;
				board[currentRow][currentCol] = 0;
				currentRow = nextRow;
				currentCol = nextCol;
			}
		}
	}
	
	/**
	 * Checks the if the player has visited every space when the time runs out.
	 */
	public final void gameFinish() {
		int unscan = 0;
		for (int row = 0; row < BOARD_ROWS; row++) {
			for (int col = 0; col < BOARD_COLS; col++) {
				if (board[row][col] == UNSCANNED) {
					unscan = unscan + 1;
				}
			}
		}
		if (unscan >= 1) {
			gameStatus = Status.LOSER;
		} else {
			gameStatus = Status.WINNER;
		}
	}
	
	/**
	 * Sets the game back to the initial conditions.
	 */
	public final void reset() {
		gameStatus = Status.PLAYING;
		currentRow = PLAYER_START;
		nextRow = currentRow;
		currentCol = PLAYER_START;
		nextCol = currentCol;
		board = createBoard();
	}
	
	/**
	 * Returns the value of the board at a location.
	 * 
	 * @param row The row to check.
	 * @param col The column to check.
	 * 
	 * @return board[row][col] The value at that location.
	 */
	public final int getBoardData(final int row, final int col) {
		return board[row][col];
	}
	
	/**
	 * sets the value of the board at a location.
	 * 
	 * @param row The row to set.
	 * @param col The column to set.
	 * @param value The value to set the space to.
	 */
	public final void setBoardData(final int row, final int col,
			final int value) {
		this.board[row][col] = value;
	}
	
	/**
	 * Returns the current row the player occupies.
	 * 
	 * @return currentRow The player's row.
	 */
	public final int getNextRow() {
		return nextRow;
	}
	
	/**
	 * Sets the player's row.
	 * 
	 * @param row The row to import.
	 */
	public final void setNextRow(final int row) {
		this.nextRow = row;
	}
	
	/**
	 * Returns the current column the player occupies.
	 * 
	 * @return currentCol The player's column.
	 */
	public final int getNextCol() {
		return nextCol;
	}
	
	/**
	 * Sets the player's column.
	 * 
	 * @param col The column to import.
	 */
	public final void setNextCol(final int col) {
		this.nextCol = col;
	}
	
	/**
	 * Returns the current game status.
	 * 
	 * @return gameStatus The game's status.
	 */
	public final Status getGameStatus() {
		return gameStatus;
	}
	
	/**
	 * Sets the player's status.
	 * 
	 * @param stat The status to import.
	 */
	public final void setGameStatus(final Status stat) {
		this.gameStatus = stat;
	}
	
	/**
	 * Returns the size of a border element.
	 * 
	 * @param array The array to pull the data from.
	 * @param row The row of the data.
	 * @param col The column of the data.
	 * @return The appropriate value for the border.
	 */
	public final int getArrays(final int array, final int row, final int col) {
		int value = (BOARD_ROWS * row) + col;
		if (array == 0) {
			return upBorder[value];
		} else if (array == 1) {
			return leftBorder[value];
		} else if (array == 2) {
			return downBorder[value];
		} else {
			return rightBorder[value];
		}
	}
}