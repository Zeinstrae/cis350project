package dialogues;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;


/*****************************************************************
 * @author Colin Smith
 * @version 12/05/2016
 *    Class designed to inform the player as to what is happening 
 *    to the main characters Vince and Howard. Displays an image
 *    and a few lines of text to convey information.
 ****************************************************************/

@SuppressWarnings("serial")
public class FishAtBlackLake extends JPanel {

	/** GUI frame. */
	private JFrame myGUI;

	/** Creates panel for image to be inserted. */
	private JPanel imagePanel;

	/** Creates button to advance dialogue. */
	private JButton continueOn;

	/** Creates Button to skip dialogue and advance game. */
	private JButton skipOn;

	/** Text Area for dialogue. */
	private JTextArea textBox;

	/** Creates panel for the skip button. */
	private JPanel textButton;
	
	/** GUI x SIZE. */
	private static final int XDIMEN = 1600;

	/** GUI y SIZE. */
	private static final int YDIMEN = 1200;
	
	/** Text Area Size. */
	private static final int TEXTAREA = 10;
	
	/** Font Size. */
	private static final int FONTSIZE = 18;
	
	/** Font Size. */
	private static final int ARRAYSIZE = 11;

	/*****************************************************************
	 * Main Method - creates new fishAtBlackLake Object.
	 * 
	 * @param args Arguments.
	 ****************************************************************/ 
	public static void main(final String[] args) {
		new FishAtBlackLake();
	}

	/*****************************************************************
	 * constructor installs all of the GUI components.
	 ****************************************************************/    
	public FishAtBlackLake() {
		// establish the frame
		myGUI = new JFrame();
		myGUI.setPreferredSize(new Dimension(XDIMEN, YDIMEN));
		myGUI.setTitle("Old Gregg");

		//imports image
		ImageIcon image = new ImageIcon("2 Fish_at_BlackLake.PNG");
		JLabel label = new JLabel("", image, JLabel.CENTER);

		//create panel for image
		imagePanel = new JPanel(new BorderLayout());
		imagePanel.setBackground(Color.BLACK);
		imagePanel.add(label, BorderLayout.CENTER);

		myGUI.add(BorderLayout.NORTH, imagePanel);

		//creates text area for GUI
		textBox = new JTextArea(TEXTAREA, TEXTAREA);
		textBox.setEditable(false);
		textBox.setText("- Ramsey: Whenever I hit a creative drought,");
		JScrollPane scrollPane = new JScrollPane(textBox);
		scrollPane.setVerticalScrollBarPolicy(
				JScrollPane.VERTICAL_SCROLLBAR_NEVER);
		myGUI.add(BorderLayout.CENTER, scrollPane);

		//used for testing purposes
		textBox.setForeground(Color.BLACK);
		textBox.setBackground(Color.RED);
		textBox.setFont(new Font("Nirmala UI Semilight", Font.PLAIN, FONTSIZE));

		//adds buttons to bottom of JPanel
		textButton = new JPanel(new BorderLayout());

		continueOn = new JButton("Advance Dialogue");
		textButton.add(BorderLayout.WEST, continueOn);

		skipOn = new JButton("Skip Dialogue");
		textButton.add(BorderLayout.EAST, skipOn);

		myGUI.add(BorderLayout.SOUTH, textButton);

		// register the listeners
		ButtonListener listener = new ButtonListener();
		continueOn.addActionListener(listener);
		skipOn.addActionListener(listener);

		myGUI.pack();
		myGUI.setVisible(true);
	}

	/*****************************************************************
	 * This method is called when any button is clicked.  The proper
	 * internal method is called as needed.
	 ****************************************************************/
	private class ButtonListener implements ActionListener {

		/** text box array. */
		private TextBoxIterator text = new TextBoxIterator();

		/** its magical. */
		private int i = 0;

		/*****************************************************************
		 * This method is called when any button is clicked.  The proper
		 * internal method is called as needed.
		 * 
		 * @param e the event that was fired.
		 ****************************************************************/ 
		public void actionPerformed(final ActionEvent e) {

			// extract the button that was clicked
			JComponent buttonPressed = (JComponent) e.getSource();

			text.setFishAtBlackLake();

			if (buttonPressed == continueOn) {
				if (i < ARRAYSIZE) {
					textBox.setText(text.getFishAtBlackLake(i));
					i++;
				} else {
					continueOn.setEnabled(false);
				}
			}

			if (buttonPressed == skipOn) {
			new OldGreggFishingLaunch();
			//	System.exit(1);
			}
		}
	}
}