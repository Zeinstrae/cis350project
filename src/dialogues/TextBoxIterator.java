package dialogues;

import java.util.ArrayList;

/*****************************************************************
 * @author Colin Smith 
 * @version 12/05/2016
 * 
 * Class designed to iterate through an array of text for each
 * text box
 ****************************************************************/
public class TextBoxIterator {
  /**ArrayList containing all of the sentences that will be
   * referenced in each text field.*/
  private ArrayList<String> fishAtBlackLake, 
  doNotFishAtBlackLake, olGreggsPlace, escapeOlGregg;
  
  /** Case Number. */
  private static final int ONE = 1;
  
  /** Case Number. */
  private static final int TWO = 2;
  
  /** Case Number. */
  private static final int THREE = 3;
  
  /** Case Number. */
  private static final int FOUR = 4;
  
  /** Case Number. */
  private static final int FIVE = 5;
  
  /** Case Number. */
  private static final int SIX = 6;
  
  /** Case Number. */
  private static final int SEVEN = 7;
  
  /** Case Number. */
  private static final int EIGHT = 8;
  
  /** Case Number. */
  private static final int NINE = 9;
  
  /** Case Number. */
  private static final int TEN = 10;
  
  /** Case Number. */
  private static final int ELEVEN = 11;
  
  /** Case Number. */
  private static final int TWELVE = 12;
  
  /** Case Number. */
  private static final int THIRTEEN = 13;
  
  
  /*******************************************************************
  Constructor.
  Generic constructor with no parameters.

  @param n/a
  ******************************************************************/
  public TextBoxIterator() {
  }
  
  
  /*******************************************************************
  Instantiates and occupies an ArrayList of strings for text
  cutscenes.

  @param n/a
  ******************************************************************/
  public final void setFishAtBlackLake() {
    fishAtBlackLake = new ArrayList<String>();
    
    //populates the ArrayList, fishAtBlackLake.
    for (int i = 0; i <= TEN; i++) {
      switch(i) {

      case 0: fishAtBlackLake.add("I go fishing out on Black Lake.");
              break;
      case ONE: fishAtBlackLake.add("With the wind "
      		+ "in your hair and the water flowing.");
              break;
      case TWO: fishAtBlackLake.add("It's perfect for inspiration.");
              break;
      case THREE: fishAtBlackLake.add("- Howard: That"
      		+ " sounds great. We could do that tomorrow.");
              break;
      case FOUR: fishAtBlackLake.add("- Ramsey: Let "
      		+ "me let you in on a little secret... gather round.");
              break;
      case FIVE: fishAtBlackLake.add("The best time "
      		+ "to go out on Black Lake is right now;");
              break;
      case SIX: fishAtBlackLake.add("when the moon is full.");
              break;
      case SEVEN: fishAtBlackLake.add("You and your "
      		+ "lady friend can have a special time together.");
              break;
      case EIGHT: fishAtBlackLake.add("I'll even"
      		+ " give you a discount. 40 euro.");
              break;
      case NINE: fishAtBlackLake.add("- Howard: It's a deal.");
              break;
      case TEN: fishAtBlackLake.add("- Ramsey: The"
      		+ " boat's tied outside on the jetty.");
              break;
      default: fishAtBlackLake.add("Invalid Parameter");
              break;
      }
    }
  }
  
  /*******************************************************************
  Returns a string called from a specific spot in the ArrayList.

  @param i integer.
  @return string from array at specified location
  ******************************************************************/
  public final String getFishAtBlackLake(final int i) {
    return fishAtBlackLake.get(i);
  }
  
  /*******************************************************************
  Instantiates and occupies an ArrayList of strings for text
  cutscenes.

  @param n/a
  ******************************************************************/
  public final void setDoNotFishAtBlackLake() {
    doNotFishAtBlackLake = new ArrayList<String>();
  //populates the ArrayList, doNotFishAtBlackLake.
    for (int i = 0; i <= ELEVEN; i++) {
      switch(i) {
      case 0: doNotFishAtBlackLake.add("- Sailor: What did you say, boy?");
              break;
      case ONE: doNotFishAtBlackLake.add("- Vince: Black Lake?");
              break;
      case TWO: doNotFishAtBlackLake.add("- Sailor: You should never go out on "
      		  + "Black Lake when the moon be full.");
              break;
      case THREE: doNotFishAtBlackLake.add("- Vince: Why?");
              break;
      case FOUR: doNotFishAtBlackLake.add("- Sailor: Because there's "
      		  + "something out there. Something evil.");
              break;
      case FIVE: doNotFishAtBlackLake.add("Something that goes by the "
      		  + "name of Old Gregg.");
              break;
      case SIX: doNotFishAtBlackLake.add("- Vince: Who?");
              break;
      case SEVEN: doNotFishAtBlackLake.add("-"
      		+ " Sailor: Old Gregg. Legendary Fish. "
      		  + "Some say he's half man, half fish.");
              break;
      case EIGHT: doNotFishAtBlackLake.add("Others say it's more of "
      		  + "a seventy-thirty split.");
              break;
      case NINE: doNotFishAtBlackLake.add("Whatever the percetage, "
      		  + "he's one fishy bastard.");
              break;
      case TEN: doNotFishAtBlackLake.add("Some say he's acquired the "
      		  + "taste of human meat.");
              break;
      case ELEVEN: doNotFishAtBlackLake.add("The only way to hook him, "
      		  + "is with a child's toe...");
              break;
      default: doNotFishAtBlackLake.add("Invalid Parameter");
              break;
      }
    }
  }
  
  /*******************************************************************
  Returns a string called from a specific spot in the ArrayList.

  @param i Integer.
  @return string from array at specified location
  ******************************************************************/
  public final String getDoNotFishAtBlackLake(final int i) {
    return doNotFishAtBlackLake.get(i);
  }
 
  /*******************************************************************
  Instantiates and occupies an ArrayList of strings for text
  cutscenes.

  @param n/a
  ******************************************************************/
  public final void setOlGreggsPlace() {
    olGreggsPlace = new ArrayList<String>();
  //populates the ArrayList, olGreggsPlace.
    for (int i = 0; i <= THIRTEEN; i++) {
      switch(i) {
      case 0: olGreggsPlace.add("- Old Gregg: I'm Old Gregg.");
              break;
      case ONE: olGreggsPlace.add("- Howard: What?");
              break;
      case TWO: olGreggsPlace.add("- Old Gregg: I'm Old Gregg!");
              break;
      case THREE: olGreggsPlace.add("- Howard: Where am I?");
              break;
      case FOUR: olGreggsPlace.add("- Old Gregg: Gregg's place. "
      		+ "You've been asleep. Do you want a little drinky?");
              break;
      case FIVE: olGreggsPlace.add("I'll get you a drink."
      		+ " You like Bailey's? Mmmm... creamy. Soft, creamy beige.");
              break;
      case SIX: olGreggsPlace.add("- Howard: Mmmm... delicious.");
              break;
      case SEVEN: olGreggsPlace.add("- Old Gregg: Do you like"
      		+ " Old Gregg's place? I've got all things that are good.");
              break;
      case EIGHT: olGreggsPlace.add("- Howard: "
      		+ "You've done some nice things with it.");
              break;
      case NINE: olGreggsPlace.add("- Howard: Well, is "
      		+ "this the way ... out? Uhm, I better be scootin'.");
              break;
      case TEN: olGreggsPlace.add("Got meetings and a "
      		+ "friend of mine is waiting, so perhaps I should be...");
              break;
      case ELEVEN: olGreggsPlace.add("- Old Gregg: Why are "
      		+ "you going? We got everything we need here. "
      		+ "We got Bailey's... creamy.");
              break;
      case TWELVE: olGreggsPlace.add("I'll get you another Bailey's.");
              break;
      case THIRTEEN: olGreggsPlace.add("- Howard: I'm fine, thanks.");
              break;
      default: olGreggsPlace.add("Invalid Parameter");
              break;
      }
    }
  }
  
  /*******************************************************************
  Returns a string called from a specific spot in the ArrayList.

  @param i i want to fly.
  @return string from array at specified location
  ******************************************************************/
  public final String getOlGreggsPlace(final int i) {
    return olGreggsPlace.get(i);
  }
  
  /*******************************************************************
  Instantiates and occupies an ArrayList of strings for text
  cutscenes.

  @param n/a
  ******************************************************************/
  public final void setEscapeOlGregg() {
    escapeOlGregg = new ArrayList<String>();
  //populates the ArrayList, escapeOlGregg.
    for (int i = 0; i <= SEVEN; i++)  {
      switch(i) {
      case 0: escapeOlGregg.add("- Howard: Oh, thank God!");
              break;
      case ONE: escapeOlGregg.add("- Vince: Quick, get in the sub!");
              break;
      case TWO: escapeOlGregg.add("- Howard: Just a minute. *grabs funk*");
              break;
      case THREE: escapeOlGregg.add("- Howard:"
      		+ " I didn't know you had one of these.");
              break;
      case FOUR: escapeOlGregg.add("- Vince: Yeah, I got it second-hand.");
              break;
      case FIVE: escapeOlGregg.add("What's in that box?");
              break;
      case SIX: escapeOlGregg.add("- Howard: This, "
      		+ "my friend, is something that's"
      		+ "going to put us back on the musical map.");
              break;
      case SEVEN: escapeOlGregg.add("- Vince: Right, full steam ahead!");
              break;
      default: escapeOlGregg.add("Invalid Parameter");
              break;
      }
    }
  }
  
  /*******************************************************************
  Returns a string called from a specific spot in the ArrayList.

  @param i i see you.
  @return string from array at specified location
  ******************************************************************/
  public final String getEscapeOlGregg(final int i) {
    return escapeOlGregg.get(i);
  }
}
