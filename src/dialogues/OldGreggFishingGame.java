package dialogues;

import java.util.Random;

/**
 * Code for the fishing game.
 */
public class OldGreggFishingGame {
	/** 2D array used to represent the game. */
	private int[][] board;
	/** The current row the player occupies. */
	private int currentRow;
	/** The current column the player occupies. */
	private int currentCol;
	/** The row to move the player to. */
	private int nextRow;
	/** The column to move the player to. */
	private int nextCol;
	/** The number of fish the player has. */
	private int playerFish;
	/** The number of fish the AI has. */
	private int aiFish;
	/** The current number of fish on the board. */
	private int currentFish;
	/** The current status of the game. */
	private Status gameStatus;
	/** The total number of rows in the board. */
	private static final int BOARD_ROWS = 10;
	/** The total number of columns in the board. */
	private static final int BOARD_COLS = 10;
	/** The amount of fish in the board at any one time. */
	private static final int TOTAL_FISH = 3;
	/** The starting location for the player. */
	private static final int PLAYER_START = 5;
	/** The value for the player. */
	private static final int HOWARD = 1;
	/** The value for a fish. */
	private static final int FISH = 2;
	/** The value for a space where the player is on top of a fish. */
	private static final int FISHABLE = 3;
	
	/**
	 * Creates the starting conditions.
	 */
	public OldGreggFishingGame() {
		gameStatus = Status.PLAYING;
		currentRow = PLAYER_START;
		nextRow = currentRow;
		currentCol = PLAYER_START;
		nextCol = currentCol;
		currentFish = 0;
		aiFish = 0;
		board = createBoard();
	}
	
	/**
	 * Builds the game board.
	 * 
	 * @return newBoard The board for the game.
	 */
	public final int[][] createBoard() {
		int[][] newBoard = new int[BOARD_ROWS][BOARD_COLS];
		newBoard[currentRow][currentCol] = HOWARD;
		for (int i = currentFish; i < TOTAL_FISH; i++) {
			int row = placeFishRow();
			int col = placeFishCol();
			if (newBoard[row][col] == 0) {
				newBoard[row][col] = FISH;
			} else {
				i--;
			}
		}
		return newBoard;
	}
	
	/**
	 * Places fish onto the board.
	 */
	public final void placeFish() {
		if (currentFish < TOTAL_FISH) {
			int row = placeFishRow();
			int col = placeFishCol();
			if (board[row][col] == 0) {
				board[row][col] = FISH;
			} else {
				placeFish();
			}
		}
	}
	
	/**
	 * Determines the row to place a fish on.
	 * 
	 * @return row The row to place the fish on.
	 */
	public final int placeFishRow() {
		Random rand = new Random();
		int row = rand.nextInt(BOARD_ROWS);
		return row;
	}
	
	/**
	 * Determines the column to place a fish on.
	 * 
	 * @return col The column to place the fish on.
	 */
	public final int placeFishCol() {
		Random rand = new Random();
		int col = rand.nextInt(BOARD_COLS);
		return col;
	}
	
	/**
	 * Moves the player through the board.
	 */
	public final void move() {
		if (board[currentRow][currentCol] == FISHABLE) {
			board[nextRow][nextCol] = HOWARD;
			board[currentRow][currentCol] = FISH;
			currentRow = nextRow;
			currentCol = nextCol;
		} else if (board[nextRow][nextCol] == FISH) {
			board[nextRow][nextCol] = FISHABLE;
			board[currentRow][currentCol] = 0;
			currentRow = nextRow;
			currentCol = nextCol;
		} else if (board[nextRow][nextCol] == 0) {
			board[nextRow][nextCol] = HOWARD;
			board[currentRow][currentCol] = 0;
			currentRow = nextRow;
			currentCol = nextCol;
		}
	}
	
	/**
	 * Checks the counts when time is up.
	 */
	public final void gameFinish() {
		if (playerFish >= aiFish) {
			gameStatus = Status.LOSER;
		} else {
			gameStatus = Status.WINNER;
		}
	}
	
	/**
	 * Sets the game back to the initial conditions.
	 */
	public final void reset() {
		gameStatus = Status.PLAYING;
		currentRow = PLAYER_START;
		nextRow = currentRow;
		currentCol = PLAYER_START;
		nextCol = currentCol;
		currentFish = 0;
		aiFish = 0;
		board = createBoard();
	}
	
	/**
	 * Returns the value of the board at a location.
	 * 
	 * @param row The row to check.
	 * @param col The column to check.
	 * 
	 * @return board[row][col] The value at that location.
	 */
	public final int getBoardData(final int row, final int col) {
		return board[row][col];
	}
	
	/**
	 * sets the value of the board at a location.
	 * 
	 * @param row The row to set.
	 * @param col The column to set.
	 * @param value The value to set the space to.
	 */
	public final void setBoardData(final int row, final int col,
			final int value) {
		this.board[row][col] = value;
	}
	
	/**
	 * Returns the current row the player occupies.
	 * 
	 * @return currentRow The player's row.
	 */
	public final int getNextRow() {
		return nextRow;
	}
	
	/**
	 * Sets the player's row.
	 * 
	 * @param row The row to import.
	 */
	public final void setNextRow(final int row) {
		this.nextRow = row;
	}
	
	/**
	 * Returns the current column the player occupies.
	 * 
	 * @return currentCol The player's column.
	 */
	public final int getNextCol() {
		return nextCol;
	}
	
	/**
	 * Sets the player's column.
	 * 
	 * @param col The column to import.
	 */
	public final void setNextCol(final int col) {
		this.nextCol = col;
	}
	
	/**
	 * Returns the current game status.
	 * 
	 * @return gameStatus The game's status.
	 */
	public final Status getGameStatus() {
		return gameStatus;
	}
	
	/**
	 * Sets the player's status.
	 * 
	 * @param stat The status to import.
	 */
	public final void setGameStatus(final Status stat) {
		this.gameStatus = stat;
	}
	
	/**
	 * Returns the current number of fish the player has.
	 * 
	 * @return playerFish The number of fish.
	 */
	public final int getFish() {
		return playerFish;
	}
	
	/**
	 * Sets the player's fish count.
	 * 
	 * @param fish The number of fish.
	 */
	public final void setFish(final int fish) {
		this.playerFish = fish;
	}
	
	/**
	 * Returns the current number of fish on the board.
	 * 
	 * @return playerFish The number of fish.
	 */
	public final int getCurrentFish() {
		return currentFish;
	}
	
	/**
	 * Sets the number of fish on the board.
	 * 
	 * @param fish The number of fish.
	 */
	public final void setCurrentFish(final int fish) {
		this.currentFish = fish;
	}
	
	/**
	 * Returns the ai's fish count.
	 * 
	 * @return aiFish The number of fish.
	 */
	public final int getAIFish() {
		return aiFish;
	}
	
	/**
	 * Sets the ai's fish count.
	 * 
	 * @param fish The number of fish.
	 */
	public final void setAIFish(final int fish) {
		this.aiFish = fish;
	}
}
