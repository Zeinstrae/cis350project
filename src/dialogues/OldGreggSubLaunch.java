package dialogues;

import java.awt.Dimension;
import javax.swing.JFrame;

/**
 * Main method.
 */
public class OldGreggSubLaunch {

	/**
	 * Launches sub game.
	 */
	public OldGreggSubLaunch() {
		final int width = 1450;
		final int height = 875;
		JFrame frame = new JFrame("Escape with the FUNK!");
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
			frame.getContentPane().add(new OldGreggSubView());
			
			frame.setPreferredSize(new Dimension(width, height));
			frame.setSize(frame.getPreferredSize());
			frame.setVisible(true);
	}

}