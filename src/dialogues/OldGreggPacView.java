package dialogues;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.Timer;

/**
 * The view for the sub search game.
 */
@SuppressWarnings("serial")
public class OldGreggPacView extends JPanel {

  /** Next game in the line of games. */
  private EscapeOlGregg blah;

  /** The board to display. */
  private JLabel[][] board;
  /** The timer for movement. */
  private Timer javaTimer;
  /** The timer listener for delay. */
  private TimerListener timer;
  /** The panels for the display. */
  private JPanel boardPanel, gamePanel;
  /** The panels for the display. */
  private JPanel scorePanel;
  /** Instance of the game class. */
  private OldGreggPacGame game;
  /** The number of rows to display. */
  private static final int ROWS = 10;
  /** The number of columns to display. */
  private static final int COLS = 10;
  /** The number of milliseconds to delay by. */
  private static final int DELAY = 500;
  /** The number of pixels to space GUI elements by. */
  private static final int SPACING = 20;
  /** The width of a label. */
  private static final int LABEL_WIDTH = 75;
  /** The height of a label. */
  private static final int LABEL_HEIGHT = 50;
  /** The size of the edge borders. */
  private static final int BORDER_SIZE = 6;
  /** The position the player starts each round at. */
  private static final int STARTING_POSITION = 5;
  /** The border on the right of the cell. */
  private static final int RIGHT = 3;
  /** THe maximum time intervals. */
  private static final int MAX_TIMER = 150;
  /** The number of iterations completed. */
  private static int count = 0;
  /** The current row the player is on. */
  private static int boatrow;
  /** The current column the player is on. */
  private static int boatcol;

  /**
   * Creates the starting view.
   */
  public OldGreggPacView() {
    game = new OldGreggPacGame();

    boatrow = STARTING_POSITION;
    boatcol = STARTING_POSITION;
    
    boardPanel = new JPanel();
    boardPanel.setLayout(new GridLayout(ROWS, COLS));
    board = new JLabel[ROWS][COLS];

    KeyListener listener = new Key();
    JTextField textField = new JTextField();
    textField.addKeyListener(listener);
    
    timer = new TimerListener();
    javaTimer = new Timer(DELAY, timer);

    scorePanel = new JPanel();
    scorePanel.add(textField);

    for (int row = 0; row < ROWS; row++) {
      for (int col = 0; col < COLS; col++) {
    	int up = game.getArrays(0, row, col);
    	int left = game.getArrays(1, row, col);
    	int down = game.getArrays(2, row, col);
    	int right = game.getArrays(RIGHT, row, col);
        board[row][col] = new JLabel("");
        board[row][col].setPreferredSize(new Dimension(LABEL_WIDTH,
            LABEL_HEIGHT));
        board[row][col].setOpaque(true);
        board[row][col].setBorder(BorderFactory.createMatteBorder(up, left,
				down, right, Color.BLACK));
        boardPanel.add(board[row][col]);
        boardPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK,
            BORDER_SIZE));
      }
    }
    displayBoard();

    gamePanel = new JPanel();
    gamePanel.setLayout(new BoxLayout(gamePanel, BoxLayout.Y_AXIS));
    gamePanel.add(boardPanel);
    gamePanel.add(Box.createVerticalStrut(SPACING));
    gamePanel.add(scorePanel);
    add(gamePanel);
    javaTimer.start();
  }

  /**
   * Displays the selected rows and columns.
   */
  private void displayBoard() {
    for (int col = 0; col < COLS; col++) {
      for (int row = 0; row < ROWS; row++) {
        if (game.getBoardData(row, col) == 0) {
          board[row][col].setBackground(Color.BLUE);
        } else if (game.getBoardData(row, col) == 1) {
          board[row][col].setBackground(Color.GREEN);
        } else if (game.getBoardData(row, col) == 2) {
          board[row][col].setBackground(Color.YELLOW);
        }
      }
    }
  }

  /**
   * Runs the time based actions.
   */
  private class TimerListener implements ActionListener {
    /**
     * Does an action based on the timer.
     * 
     * @param e The event that triggers the listener.
     */
    public void actionPerformed(final ActionEvent e) {
      if (game.getGameStatus() == Status.PLAYING) {
        count++;
        if (count == MAX_TIMER) {
          game.gameFinish();
        }
        displayBoard();
      }
      if (game.getGameStatus() == Status.WINNER) {
        javaTimer.stop();
        JOptionPane.showMessageDialog(null,
            "You found the cave."
                + "\n What could be inside?");
        blah = new EscapeOlGregg();

      }
      if (game.getGameStatus() == Status.LOSER) {
        JOptionPane.showMessageDialog(null, "You failed"
            + "to find the cave in time. Try again.");
        count = 0;
        game.reset();
      }
    }
  }

  /**
   * Runs the keyboard based actions.
   */
  private class Key implements KeyListener {
	
	/**
	* Does something when a key is released. Should execute no code.
	* 
	* @param e The key that was pressed
	*/
	@Override
	public void keyReleased(final KeyEvent e) {
		//This method should be empty
	}

	/**
	* Does something when a key is pressed.
	* 
	* @param e The key that was pressed
	*/
	@Override
	public void keyPressed(final KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_UP) {
			if (boatrow > 0) {
				boatrow--;
		        game.setNextRow(boatrow);
		        game.setNextCol(boatcol);
		        game.move();
			}
		} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
			if (boatrow < ROWS - 1) {
				boatrow++;
		        game.setNextRow(boatrow);
		        game.setNextCol(boatcol);
		        game.move();
		    }
		} else if (e.getKeyCode() == KeyEvent.VK_LEFT) { 
	        if (boatcol > 0) {
	       		boatcol--;
		        game.setNextRow(boatrow);
		        game.setNextCol(boatcol);
		        game.move();
		    }
		} else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
	        if (boatcol < ROWS - 1) {
	        	boatcol++;
		        game.setNextRow(boatrow);
		        game.setNextCol(boatcol);
		        game.move();
		    }
		}
	}

	/**
	* Does something when a printing character is pressed.
	* Should execute no code.
	* 
	* @param e The key that was pressed
	*/
	@Override
	public void keyTyped(final KeyEvent e) {
		// This method should be empty
	}
  }
}