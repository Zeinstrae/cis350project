package dialogues;
/*********************************************************************
 * This Classhelp the mazes by creating nodes that have information in them.
 * These nodes can have a wall, hidden item, or nothing at all.
 * @author Brentan Chesla
 * @version 10/18/16
 */
public class Spotholder {
  
  /** Holder to tell if wall exists. */
  private Boolean wall;
  
  /** Holder to tell if item exists. */
  private Boolean hiddenItem;

  
  /******************************************************
   * Constructor Method instantiate wall and hiddenitem.
   */
  public Spotholder() {
    wall = false;
    hiddenItem = false;
  }


  /*****************************************************
   *  Get Method ask if item exists.
   * 
   * @return hiddenItem Tells if item exists
   */
  public final Boolean getItem() {
    return hiddenItem;
  }
  
  /*****************************************************
   *  Get Method ask if wall exists.
   * 
   * @return wall Tells if wall exists.
   */
  public final Boolean getWall() {

    return wall;
  }

  
  /*****************************************************
   *  Set Method to change the wall existance.
   *  @param newwall Changes wall to true or false.
   */
  public final void setWall(final Boolean newwall) {
    this.wall = newwall;
  }

  /*****************************************************
   *  Set Method to change the item existance.
   *  @param newItem Changes item to true or false.
   */
  public final void setItem(final Boolean newItem) {
    this.hiddenItem = newItem;
  }
}
