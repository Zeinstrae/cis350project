package dialogues;


import java.awt.FlowLayout;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.WindowConstants;



/*********************************************************************
 * This has the user play a game that makes forces the user to try and
 * please a crowd. He/she must beat 2 million in 30 seconds or they lose.
 * @author Brentan Chesla
 * @version 10/18/16
 */
@SuppressWarnings("serial")
public class PressButton extends JFrame implements ActionListener {

  /** text field to show user how many clicks they did. */
  private TextField text;

  /** end the game. **/
  private int endit;

  /** button to iterate the clicks. */
  private JButton pressme;

  /** number of clicks done by user. */
  private int numClicks;

  /** Timer to keep time. */
  private Timekeeper timer;

  /** Size of text box. */
  private static final int TEXT_FIELD = 20;

  /** Clicks to win. */
  private static final int CLICKS_TO_WIN = 200000;

  /** Seconds to play. */
  private static final int SECONDS_TO_PLAY = 2;

  /** Size of next play. */
  private static final int SIZEX = 350;

  /** Size of next play. */
  private static final int SIZEY = 100;

  /*********************************************************************
   * This is the constructor method that accepts a users param
   * request and builds the game.
   * @param title this is the title of the frames
   */
  public PressButton(final String title) {
    super(title);
    //
    //    setSize(SIZEX, SIZEY);
    //    setVisible(true);
    System.out.println("So you think Howard has the skill now???");
    System.out.println("Lets see how well Howard can play.");
    System.out.println("Can Howard please the crowd??");

    endit = 0;
    timer = new Timekeeper();
    text = new TextField(TEXT_FIELD);
    numClicks = 0;
    pressme = new JButton("Are you ready to rock!!");
    setLayout(new FlowLayout());
    setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    add(text);
    add(pressme);
    pressme.addActionListener(this);



  }

  /***
   * For testing purposes to end the game.
   */
  public final void setendit() {
    endit = SECONDS_TO_PLAY + SECONDS_TO_PLAY;
  }

  /****
   * click the button for testing.
   */
  public final void clickit() {
    pressme.doClick();
  }

  /**
   * Returns number of clicks.
   * @return numClicks returns total clicks
   */
  public final int getnumclick() {
    return numClicks;
  }
  /***
   * For testing purposes to end the game.
   */
  public final void setwinclick() {
    numClicks = CLICKS_TO_WIN;
  }
  /*********************************************************************
   * This is our action listener that counts how many clicks a user has.
   * However, if the user cannot play well enough, he/she will lose.
   * @param playingfortheLadies the action of clicking the button
   */
  public final void actionPerformed(final ActionEvent playingfortheLadies) {


    numClicks++;
    if (numClicks == 1) {
      timer.start();
    }
    if (endit > SECONDS_TO_PLAY && !(numClicks > CLICKS_TO_WIN)) {
      setVisible(false); //you can't see me!
      dispose(); //Destroy the JFrame object
      System.out.println("Howard still sucks!");
      System.out.println("That's okay though, "
          + "Howard will just go down and have a vacation.....");

      new TravelToBlackLake();
      //System.exit(0);
    }
    endit = timer.getSeconds();
    if (numClicks >= CLICKS_TO_WIN) {
      System.out.println("Howard may be cool, but "
          + "he's not cool enough to be popular");
      System.out.println("That's okay though, "
          + "Howard will just go down and have a vacation.....");
      new TravelToBlackLake();
    }
    text.setText("Ladies in audience: " + numClicks);
  }
  /********
   * 
   * Main method to run game.
   * @param args args to run game.
   */
  public static void main(final String[] args) {
    PressButton play = new PressButton("Neat");
    play.setSize(SIZEX, SIZEY);
    play.setVisible(true);
  }
}