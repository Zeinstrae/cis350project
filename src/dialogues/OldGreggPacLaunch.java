package dialogues;

import java.awt.Dimension;
import javax.swing.JFrame;

/**
 * Main method.
 */
public class OldGreggPacLaunch {

	/**
	 * Launches game to find the cave.
	 */
	public OldGreggPacLaunch() {
		final int width = 1450;
		final int height = 875;
		JFrame frame = new JFrame("Find the cave!");
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
			frame.getContentPane().add(new OldGreggPacView());
			
			frame.setPreferredSize(new Dimension(width, height));
			frame.setSize(frame.getPreferredSize());
			frame.setVisible(true);
	}

}