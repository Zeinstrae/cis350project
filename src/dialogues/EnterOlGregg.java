package dialogues;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;

/*****************************************************************
 * @author Colin Smith
 * @version 12/05/2016
 * @version 2
 * Class designed to show a cutscene from the video Old Gregg
 ****************************************************************/

public class EnterOlGregg extends JPanel {

  /** GUI x SIZE. */
  private static final int XDIMEN = 250;

  /** GUI y SIZE. */
  private static final int YDIMEN = 200;
  
  /** GUI frame. */
  private JFrame myGUI;
  
  /**Creates panel for video to be inserted. */
  private JPanel videoPanel;
  
  /**Creates panel for the continue button. */
  private JPanel continueButton;
  
  /**Creates Button to continue game. */
  private JButton continueOn;

  /*****************************************************************
   * Main Method.
   * 
   * @param args Arguments.
   ****************************************************************/ 
  public static void main(final String[] args) {
    new EnterOlGregg();
  }

  /*****************************************************************
   * constructor installs all of the GUI components.
   * 
   * @param n/a
   ****************************************************************/    
  public EnterOlGregg() {
    // establish the frame
    myGUI = new JFrame();
    myGUI.setPreferredSize(new Dimension(XDIMEN, YDIMEN));
    myGUI.setTitle("Old Gregg");
    
    //adds the continue button
    continueButton = new JPanel(new BorderLayout());
    
    continueOn = new JButton("Continue");
    continueButton.add(continueOn);
    
    myGUI.add(continueButton);
    
    // register the listeners
    ButtonListener listener = new ButtonListener();
    continueOn.addActionListener(listener);
    
    myGUI.pack();
    myGUI.setVisible(true);
    
    try {
        Desktop.getDesktop().open(new File("6 Meet Ol' Greg.mp4"));
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
  }
  
  /*****************************************************************
   * This method is called when any button is clicked.  The proper
   * internal method is called as needed.
   ****************************************************************/ 
  private class ButtonListener implements ActionListener {
	  /*****************************************************************
	   * This method is called when any button is clicked.  The proper
	   * internal method is called as needed.
	   * 
	   * @param e the event that was fired
	   ****************************************************************/  
      public void actionPerformed(final ActionEvent e) {

          // extract the button that was clicked
          JComponent buttonPressed = (JComponent) e.getSource();
          
          if (buttonPressed == continueOn) {
            new OlGreggsPlace();
           // System.exit(1);
          }
      }
   }
}