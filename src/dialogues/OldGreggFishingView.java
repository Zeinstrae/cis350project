package dialogues;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.Timer;

/**
 * The view for the fishing game.
 */
@SuppressWarnings("serial")
public class OldGreggFishingView extends JPanel {

  /** The board to display. */
  private JLabel[][] board;
  /** The labels to display score. */
  private JLabel howard, hscore, vincent, vscore;
  /** The timer for movement. */
  private Timer javaTimer;
  /** The timer listener for delay. */
  private TimerListener timer;
  /** The panels for the display. */
  private JPanel boardPanel, gamePanel;
  /** The panels for the display. */
  private JPanel scorePanel, hScorePanel, vScorePanel;
  /** Instance of the game class. */
  private OldGreggFishingGame game;
  /** The number of rows to display. */
  private static final int ROWS = 10;
  /** The number of columns to display. */
  private static final int COLS = 10;
  /** The number of milliseconds to delay by. */
  private static final int DELAY = 500;
  /** The number of pixels to space GUI elements by. */
  private static final int SPACING = 20;
  /** The width of a label. */
  private static final int LABEL_WIDTH = 75;
  /** The height of a label. */
  private static final int LABEL_HEIGHT = 50;
  /** The position the player starts each round at. */
  private static final int STARTING_POSITION = 5;
  /** The size of the edge borders. */
  private static final int BORDER_SIZE = 6;
  /** The size of the internal borders. */
  private static final int IN_SIZE = 3;
  /** THe maximum time intervals. */
  private static final int MAX_TIME = 120;
  /** Denotes a space to fish on. */
  private static final int FISHABLE = 3;
  /** The number of iterations completed. */
  private static int count = 0;
  /** The current row the player is on. */
  private static int boatrow;
  /** The current column the player is on. */
  private static int boatcol;

  /**
   * Creates the starting view.
   */
  public OldGreggFishingView() {
    game = new OldGreggFishingGame();

    boatrow = STARTING_POSITION;
    boatcol = STARTING_POSITION;
    
    boardPanel = new JPanel();
    boardPanel.setLayout(new GridLayout(ROWS, COLS));
    board = new JLabel[ROWS][COLS];

    KeyListener listener = new Key();
    JTextField textField = new JTextField();
    textField.addKeyListener(listener);
    
    timer = new TimerListener();
    javaTimer = new Timer(DELAY, timer);

    scorePanel = new JPanel();
    scorePanel.setLayout(new BoxLayout(scorePanel, BoxLayout.Y_AXIS));
    hScorePanel = new JPanel();
    hScorePanel.setLayout(new BoxLayout(hScorePanel, BoxLayout.X_AXIS));
    vScorePanel = new JPanel();
    vScorePanel.setLayout(new BoxLayout(vScorePanel, BoxLayout.X_AXIS));
    howard = new JLabel("Howard's Fish: ");
    hscore = new JLabel();
    vincent = new JLabel("Vincent's Fish: ");
    vscore = new JLabel();
    hScorePanel.add(howard);
    hScorePanel.add(hscore);
    vScorePanel.add(vincent);
    vScorePanel.add(vscore);
    scorePanel.add(hScorePanel);
    scorePanel.add(Box.createVerticalStrut(SPACING));
    scorePanel.add(vScorePanel);
    scorePanel.add(textField);

    for (int row = 0; row < ROWS; row++) {
      for (int col = 0; col < COLS; col++) {
        board[row][col] = new JLabel("");
        board[row][col].setPreferredSize(new Dimension(LABEL_WIDTH,
            LABEL_HEIGHT));
        board[row][col].setOpaque(true);
        board[row][col].setBorder(BorderFactory.createLineBorder(
            Color.BLACK, IN_SIZE));
        boardPanel.add(board[row][col]);
        boardPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK,
            BORDER_SIZE));
      }
    }
    displayBoard();

    gamePanel = new JPanel();
    gamePanel.setLayout(new BoxLayout(gamePanel, BoxLayout.Y_AXIS));
    gamePanel.add(boardPanel);
    gamePanel.add(Box.createVerticalStrut(SPACING));
    gamePanel.add(scorePanel);
    add(gamePanel);
    javaTimer.start();
  }

  /**
   * Displays the selected rows and columns.
   */
  private void displayBoard() {
    for (int col = 0; col < COLS; col++) {
      for (int row = 0; row < ROWS; row++) {
        if (game.getBoardData(row, col) == 0) {
          board[row][col].setBackground(Color.BLUE);
        } else if (game.getBoardData(row, col) == 1) {
          board[row][col].setBackground(Color.ORANGE);
        } else if (game.getBoardData(row, col) == 2) {
          board[row][col].setBackground(Color.YELLOW);
        } else if (game.getBoardData(row, col) == FISHABLE) {
          board[row][col].setBackground(Color.GREEN);
        }
      }
    }
  }

  /**
   * Runs the time based actions.
   */
  private class TimerListener implements ActionListener {
    /**
     * Does an action based on the timer.
     * 
     * @param e The event that triggers the listener.
     */
    public void actionPerformed(final ActionEvent e) {
      if (game.getGameStatus() == Status.PLAYING) {
        count++;
        game.setAIFish(count);
        int hFish = game.getFish();
        hscore.setText("" + hFish);
        int vFish = game.getAIFish();
        vscore.setText("" + vFish);
        if (count == MAX_TIME) {
          game.gameFinish();
        }
        displayBoard();
      }
      if (game.getGameStatus() == Status.WINNER) {
        javaTimer.stop();
        JOptionPane.showMessageDialog(null,
            "Failed to catch more fish."
                + "\n You win.");
        new DoNotFishAtBlackLake();

      }
      if (game.getGameStatus() == Status.LOSER) {
        JOptionPane.showMessageDialog(null, "You won."
            + "\nYou weren't supposed to do that.");
        count = 0;
        game.reset();
      }
    }
  }

  /**
   * Runs the keyboard based actions.
   */
  private class Key implements KeyListener {
	
	/**
	* Does something when a key is released. Should execute no code.
	* 
	* @param e The key that was pressed
	*/
	@Override
	public void keyReleased(final KeyEvent e) {
		//This method should be empty
	}

	/**
	* Does something when a key is pressed.
	* 
	* @param e The key that was pressed
	*/
	@Override
	public void keyPressed(final KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_UP) {
			if (boatrow > 0) {
				boatrow--;
		        game.setNextRow(boatrow);
		        game.setNextCol(boatcol);
		        game.move();
			}
		} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
			if (boatrow < ROWS - 1) {
				boatrow++;
		        game.setNextRow(boatrow);
		        game.setNextCol(boatcol);
		        game.move();
		    }
		} else if (e.getKeyCode() == KeyEvent.VK_LEFT) { 
	        if (boatcol > 0) {
	       		boatcol--;
		        game.setNextRow(boatrow);
		        game.setNextCol(boatcol);
		        game.move();
		    }
		} else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
	        if (boatcol < ROWS - 1) {
	        	boatcol++;
		        game.setNextRow(boatrow);
		        game.setNextCol(boatcol);
		        game.move();
		    }
		} else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
	    	if (game.getBoardData(boatrow, boatcol) > 1) {
	    		game.setBoardData(boatrow, boatcol, 1);
	            int fish = game.getFish();
	            game.setFish(fish + 1);
	            int currentFish = game.getCurrentFish();
	            game.setCurrentFish(currentFish - 1);
	            game.placeFish();
	        }
		}
	}

	/**
	* Does something when a printing character is pressed.
	* Should execute no code.
	* 
	* @param e The key that was pressed
	*/
	@Override
	public void keyTyped(final KeyEvent e) {
		// This method should be empty
	}
  }
}