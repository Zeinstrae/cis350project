package dialogues;
import java.util.Scanner;


/*********************************************************************
 * This Program is a MiniGame that you are tasked with finding an individual,
 * named Ramsey. You are tired however and do not have a lot 
 * of steps left in you.
 * Therefore you must find Ramsey before you run out of steps.
 * @author Brentan Chesla
 * @version 10/18/16
 */
public class FindRamsey {

  /** Run the program. **/
  private Boolean keeprunning = true;
  
  /** numbering system for array. */
  private static final int ZERO = 0;

  /** numbering system for array. */
  private static final int ONE = 1;

  /** numbering system for array. */
  private static final int TWO = 2;

  /** numbering system for array. */
  private static final int THREE = 3;

  /** numbering system for array. */
  private static final int FOUR = 4;

  /** numbering system for array. */
  private static final int FIVE = 5;

  /** numbering system for array. */
  private static final int SIX = 6;

  /** making the maze. */
  private Spotholder[][] findRamsey;

  /** wall node. */
  private Spotholder wall;

  /** no wall node. */
  private Spotholder nothing;

  /** node for the item. */
  private Spotholder findthisItem;

  /** current x plane location. */
  private int currentx; 

  /** current y plane location. */
  private int currenty;

  /** out of bounds on the array. */
  private static final int OUT_OF_BOUNDS = -1;

  /** Array Size. */
  private static final int ARRAY_SIZE = 6;

  /** total number of allowed steps. */
  private int steps = ARRAY_SIZE;


  /*********************************************************************
   * This is the Constructor method that helps build the game.
   ********************************************************************/
  public FindRamsey() {
    currentx = 0;
    currenty = 0;
    findRamsey = new Spotholder[ARRAY_SIZE][ARRAY_SIZE];
    wall = new Spotholder();
    nothing = new Spotholder();
    findthisItem = new Spotholder();

    findthisItem.setItem(true);
    findthisItem.setWall(false);
    wall.setWall(true);
    createMap();
    run();

  }


  /*******************************************************************
   * This Method takes the program and sends it into a loop, asking the user
   * if he/she wants to go Left, Right, Up, or Down. It will check for
   * Walls as well as check for Ramsey. If you run out of steps however,
   * you may fall asleep out of exhaustion!
   *******************************************************************/
  public final void run() {
    String user = "";
    String valid;
    Boolean validrequest;
    Scanner sc = new Scanner(System.in);
    while (keeprunning) {
      validrequest = false;
      valid = "";
      System.out.println("Howard enters the bar but cannot find Ramsey.");
      System.out.println("He is tired from the trip and wants to sit down");
      System.out.println("He begins the search..");
      System.out.println("Options: Left, Right, Up, Down");
      if (keeprunning == true) {
      user = sc.nextLine();
      }
      
      //testing only!
      if (user.equals("Left test")) {
        keeprunning = false;
        user = "Left";
      }
      
      //testing only!
      if (user.equals("Right test step")) {
        keeprunning = false;
        user = "Right";
        steps = 0;
      }
      
      if (user.equals("done")) {
        keeprunning = false;
        validrequest = true;
        valid = "done";
        sc.close();
        
      }
      
    //testing only!
      if (user.equals("Right test")) {
        keeprunning = false;
        user = "Right";
      }
      
    //testing only!
      if (user.equals("Up test")) {
        keeprunning = false;
        user = "Up";
      }
      
    //testing only!
      if (user.equals("Down test")) {
        keeprunning = false;
        user = "Down";
      }
      
      if (user.equals("Left")) {
        valid = checkSpot(currentx - 1, currenty);
        validrequest = true;
      } else {
        if (user.equals("Right")) {
          valid = checkSpot(currentx + 1, currenty);
          validrequest = true;
        } else {
          if (user.equals("Up")) {
            valid = checkSpot(currentx, currenty - 1);
            validrequest = true;
          } else {
            if (user.equals("Down")) {
              valid = checkSpot(currentx, currenty + 1);
              validrequest = true;
            }
          }
        }
      }

      if (steps == 0) {
        System.out.println("Too tired to move, Howard passes out");
        System.out.println("He wakes up at the entrance with "
            + "shells on his clothes");
        System.out.println("He is unaware of how he got here"
            + " but knows he must find Ramsey");
        steps = SIX;
        currentx = 0;
        currenty = 0;
      } else {
        if (validrequest) {
          if (valid == "wall") {
            System.out.println("Howard attempts to move, but cannot");
            System.out.println("He is becoming tired still");
            System.out.println();
            steps--;
            System.out.println("------" + steps + "------");
          } else {
            if (valid == "nothing") {
              System.out.println("Howard moves, he is unsure of his direction");
              System.out.println("He becomes more and more tired");
              System.out.println();
              steps--;
              System.out.println("------" + steps + "------");
            } else {
              if (valid == "done") {
            	sc.close();
                steps--;
                System.out.println("Lucky for you, you have spotted Ramsey");
                System.out.println("Tired and only " + steps
                    + " steps left, you sit next to him");
                System.out.println("He has a tale to tell you..");
                System.out.println();
                keeprunning = false;
                new FishAtBlackLake();
                break;
              }
            }
          }
        } else {
          System.out.println("Howard could not understand your directions! :(");
        }
        System.out.println("--------------------------------"
            + "----------------------------");
        System.out.println(currentx + ", " + currenty);
      }
    }
  }

  /***
   * For the Tester to stop the loop.
   */
  public final void setrunfalse() {
    keeprunning = false;
  }
  /***
   * Getter Method the helps retreive current locations.
   * Used Primarily for visuals.
   * @return currentx Current x Location
   */
  public final int getx() {
    return currentx;
  }
  
  /***
   * Getter Method the helps retreive current locations.
   * Used Primarily for visuals.
   * @return currenty Current y Location
   */
  public final int gety() {
    return currenty;
  }

  /********************************************************************
   * This Method takes the users desired path and checks to see if it
   * is valid.
   * @param xlocNew This is the desired x direction.
   * @param ylocNew This is the desired y direction.
   * @return "done" if the user found Ramsey. Else "nothing" or "wall"
   */
  public final String checkSpot(final int xlocNew, final int ylocNew) {
    Spotholder temp;

    if (xlocNew < ARRAY_SIZE && xlocNew > OUT_OF_BOUNDS 
        && ylocNew < ARRAY_SIZE && ylocNew > OUT_OF_BOUNDS) {
      temp = findRamsey[xlocNew][ylocNew];

      if (!temp.getWall()) {
        currentx = xlocNew;
        currenty = ylocNew;

        //found item
        if  (temp.getItem()) {
          return "done";
        } else {

          //didn't find item
          return "nothing";
        }
      } else {
        return "wall";
      }
    } else {
      return "wall";
    }
  }

  /********************************************************************
   * This Method creates our map and hidden location of Ramsey.
   */
  public final void createMap() {

    //Column 0
    for (int y = ZERO; y < SIX; y++) {
      if (y == ONE) {
        findRamsey[ZERO][y] = wall;
      } else {
        findRamsey[ZERO][y] = nothing;
      }
    }


    //Column 1
    for (int y = ZERO; y < SIX; y++) {
      if (y == FOUR) {
        findRamsey[ONE][y] = wall;
      } else {
        findRamsey[ONE][y] = nothing;
      }
    }

    //Column 2
    for (int y = ZERO; y < SIX; y++) {
      if (y == ZERO || y == TWO) {
        findRamsey[TWO][y] = wall;
      } else {
        findRamsey[TWO][y] = nothing;
      }
    }

    //Column 3
    for (int y = ZERO; y < SIX; y++) {
      if (y == ZERO) { 
        findRamsey[THREE][y] = findthisItem;
      } else {
        findRamsey[THREE][y] = nothing;
      }
    }

    //Column 4
    for (int y = ZERO; y < SIX; y++) {
      if (y == THREE) {
        findRamsey[FOUR][y] = wall;
      } else {
        findRamsey[FOUR][y] = nothing;
      }
    }

    //Column 5
    for (int y = ZERO; y < SIX; y++) {
      if (y == ONE) {
        findRamsey[FIVE][y] = wall;
      } else {
        findRamsey[FIVE][y] = nothing;
      }
    }


  }

}