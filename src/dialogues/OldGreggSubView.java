package dialogues;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.Timer;

/**
 * The view for the sub game.
 */
@SuppressWarnings("serial")
public class OldGreggSubView extends JPanel {
	/** The board to display. */
	private JLabel[][] board;
	/** The timer for movement. */
	private Timer javaTimer;
	/** The timer listener for delay. */
	private TimerListener timer;
	/** The panels for the display. */
	private JPanel boardPanel, gamePanel, buttonPanel;
	/** Instance of the game class. */
	private OldGreggSubGame game;
	/** The number of rows to display. */
	private static final int ROWS = 5;
	/** The number of columns to display. */
	private static final int COLS = 8;
	/** The number of milliseconds to delay by. */
	private static final int DELAY = 500;
	/** The number of pixels to space GUI elements by. */
	private static final int SPACING = 20;
	/** The width of a label. */
	private static final int LABEL_WIDTH = 150;
	/** The height of a label. */
	private static final int LABEL_HEIGHT = 100;
	/** THe size of the edge borders. */
	private static final int BORDER_SIZE = 6;
	/** Represents the end scressn. */
	private static final int END = 3;
	/** The number of iterations completed. */
	private static int count = 0;
	/** The current row the player is on. */
	private static int subrow = 2;

	/**
	 * Creates the starting view.
	 */
	public OldGreggSubView() {
		game = new OldGreggSubGame();

		boardPanel = new JPanel();
		boardPanel.setLayout(new GridLayout(ROWS, COLS));
		board = new JLabel[ROWS][COLS];

	    KeyListener listener = new Key();
	    JTextField textField = new JTextField();
	    textField.addKeyListener(listener);
		
		timer = new TimerListener();
		javaTimer = new Timer(DELAY, timer);

		buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));
		buttonPanel.add(textField);


		for (int row = 0; row < ROWS; row++) {
			for (int col = 0; col < COLS; col++) {
				board[row][col] = new JLabel("");
				board[row][col].setPreferredSize(new Dimension(LABEL_WIDTH,
						LABEL_HEIGHT));
				board[row][col].setOpaque(true);
				board[row][col].setBorder(BorderFactory.createMatteBorder(2, 0,
						2, 0, Color.BLACK));
				boardPanel.add(board[row][col]);
				boardPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK,
						BORDER_SIZE));
			}
		}
		displayBoard(0);

		gamePanel = new JPanel();
		gamePanel.setLayout(new BoxLayout(gamePanel, BoxLayout.Y_AXIS));
		gamePanel.add(boardPanel);
		gamePanel.add(Box.createVerticalStrut(SPACING));
		gamePanel.add(buttonPanel);
		add(gamePanel);
		javaTimer.start();
	}

	/**
	 * Displays the selected rows and columns.
	 *
	 * @param startCol The column to start at.
	 */
	private void displayBoard(final int startCol) {
		for (int col = startCol; col < startCol + COLS; col++) {
			for (int row = 0; row < ROWS; row++) {
				if (game.getBoardData(row, col) == 0) {
					board[row][col - startCol].setBackground(Color.BLUE);
				} else if (game.getBoardData(row, col) == 1) {
					board[row][col - startCol].setBackground(Color.GRAY);
				} else if (game.getBoardData(row, col) == 2) {
					board[row][col - startCol].setBackground(Color.RED);
				} else if (game.getBoardData(row, col) == END) {
					board[row][col - startCol].setBackground(Color.BLACK);
				}
			}
		}
	}

	/**
	 * Runs the time based actions.
	 */
	private class TimerListener implements ActionListener {
		/**
		 * Does an action based on the timer.
		 *
		 * @param e The event that triggers the listener.
		 */
		public void actionPerformed(final ActionEvent e) {
			if (game.getGameStatus() == Status.PLAYING) {
				count++;
				game.setCurrentRow(subrow);
				game.setCurrentCol(count);
				game.collisionCheck();
				game.move();
				game.gameFinish();
				displayBoard(count);
			}
			if (game.getGameStatus() == Status.WINNER) {
				javaTimer.stop();
				JOptionPane.showMessageDialog(null,
						"You escaped with the funk");
			}
			if (game.getGameStatus() == Status.LOSER) {
				JOptionPane.showMessageDialog(null, "You crashed");
				count = 0;
				subrow = 2;
				game.reset();
			}
		}
	}

	/**
	* Runs the keyboard based actions.
	*/
	private class Key implements KeyListener {
	
		/**
		* Does something when a key is released. Should execute no code.
		* 
		* @param e The key that was pressed
		*/
		@Override
		public void keyReleased(final KeyEvent e) {
			//This method should be empty
		}

		/**
		* Does something when a key is pressed.
		* 
		* @param e The key that was pressed
		*/
		@Override
		public void keyPressed(final KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_UP) {
				if (subrow > 0) {
					subrow--;
				}
			} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
				if (subrow < ROWS - 1) {
					subrow++;
				}
			}
		}

		/**
		* Does something when a printing character is pressed.
		* Should execute no code.
		* 
		* @param e The key that was pressed
		*/
		@Override
		public void keyTyped(final KeyEvent e) {
			// This method should be empty
		}
	}
}